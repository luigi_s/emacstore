## Emacs Public archive
-----------------------

Find here multiple files for customizing Emacs

### How to
----------
```
# Clone this repo
 $ git clone https://bitbucket.org/luigi_s/emacstore.git ~/.emacs/emacstore

# Backup your old init.el
 $ [ -f  ~/.emacs/init.el ] && mv ~/.emacs/init.el ~/.emacs/init.el.old

# Link new init.el
 $ ln -sf ~/.emacs/emacstore/init/init-home.el ~/.emacs/init.el
```
