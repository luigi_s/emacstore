;;; init.el --- Emacs configuration

;; Copyright (C) 2019 Luigi Santivetti

;; Author: Luigi Santivetti <luigi.santivetti@gmail.com>

;; This program is free software. You can redistribute it and/or modify it under
;; the terms of the GPL, v3.
;;
;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;; FOR A PARTICULAR PURPOSE.
;;
;; See http://www.wtfpl.net/.

;;; Commentary:

;; Faces are manually edited and vaguely recall SublimeText, I find it balanced.
;; This configuration file works for me, but I DO NOT KNOW lisp, when you find
;; any problem let me know the fix :P - thanks.

;;; Code:

;;,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,;;
;; Debian Stretch Emacs default version
;;'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';;
(when (< (string-to-number (concat
			    (number-to-string emacs-major-version)
			    "."
			    (number-to-string emacs-minor-version))) 24.4)
  (error "Version >= 24.5 is required."))

;;,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,;;
;; Install packages
;;'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';;
(require 'package)
(package-initialize t)

(add-to-list 'package-archives
	     '("melpa" . "https://melpa.org/packages/") t)
(add-to-list 'package-archives
	     '("gnu" . "https://elpa.gnu.org/packages/"))

(package-initialize)

(setq package-selected-packages
      '(diff-hl
	multiple-cursors
	smooth-scroll
	markdown-mode
	smart-mode-line-powerline-theme
	rainbow-delimiters
	nlinum))

(unless package-archive-contents
  (package-refresh-contents))

(dolist (package package-selected-packages)
  (unless (package-installed-p package)
    (message "Missing packages detected, please wait...")
    (package-install package)))

;;,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,;;
;; Custom set variables
;;'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';;
(custom-set-variables
 '(column-number-mode t)
 '(cua-mode t nil (cua-base))
 '(custom-enabled-themes (quote (smart-mode-line-powerline)))
 '(custom-safe-themes
   (quote
    ("84d2f9eeb3f82d619ca4bfffe5f157282f4779732f48a5ac1484d94d5ff5b279" default)))
 '(fill-column 80)
 '(global-hl-line-mode t)
 '(nlinum-highlight-current-line t)
 '(global-nlinum-mode t)
  '(inhibit-startup-screen t)
 '(show-paren-mode t)
 '(tool-bar-mode nil))

;;,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,;;
;; Custom set faces
;;'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';;
(custom-set-faces
 '(default ((t (:background "#343D46" :foreground "#D8DEE9" :slant normal
			    :weight normal :width normal :foundry "adobe"
			    :family "Source Code Variable"))))
 '(font-lock-comment-face ((t (:background "#343D46" :foreground "#848B97"))))
 '(fringe ((t (:background "#343D46"))))
 '(hl-line ((t (:background "#2C3133" :weight bold))))
 '(nlinum-current-line ((t (:background "#2C3133" :foreground "white"
					:weight bold :height 80))))
 '(nlinum ((t (:slant normal :width ultra-condensed :foreground "#848B97"))))
 '(mode-line ((t (:background "#4E5A65" :foreground "#D8DEE9" :box nil))))
 '(mode-line-buffer-id-inactive ((t (:inherit mode-line-buffer-id
					      :background "#757B81"
					      :foreground "#D8DEE9"))))
 '(mode-line-highlight ((t (:box (:line-width 2 :color "#4E5A65"
					      :style released-button)))))
 '(mode-line-inactive ((t (:background "#4E5A65" :foreground "#4E5A65"
				       :box nil))))
 '(powerline-active1 ((t (:inherit sml/global :background "#4E5A65"
				   :foreground "#D8DEE9"))))
 '(powerline-active2 ((t (:inherit sml/global :background "#757B81"
				   :foreground "#D8DEE9"))))
 '(powerline-inactive1 ((t (:inherit mode-line-inactive
				     :background "#4E5A65"))))
 '(powerline-inactive2 ((t (:inherit mode-line-inactive
				     :background "#4E5A65"))))
 '(rainbow-delimiters-depth-1-face ((t (:foreground "dark orange"))))
 '(rainbow-delimiters-depth-2-face ((t (:foreground "deep pink"))))
 '(rainbow-delimiters-depth-3-face ((t (:foreground "chartreuse"))))
 '(rainbow-delimiters-depth-4-face ((t (:foreground "deep sky blue"))))
 '(rainbow-delimiters-depth-5-face ((t (:foreground "yellow"))))
 '(rainbow-delimiters-depth-6-face ((t (:foreground "orchid"))))
 '(rainbow-delimiters-depth-7-face ((t (:foreground "spring green"))))
 '(rainbow-delimiters-depth-8-face ((t (:foreground "sienna1"))))
 '(region ((t (:background "#4E5A65" :foreground "#D8DEE9"))))
 '(sml/col-number ((t (:inherit sml/global :background "#4E5A65"
				:foreground "white" :weight bold))))
 '(sml/filename ((t (:inherit sml/global :background "#4E5A65"
			      :foreground "black"))))
 '(sml/global ((t (:background "#4E5A65" :foreground "#D8DEE9"))))
 '(sml/line-number ((t (:inherit sml/global :background "#4E5A65"
				 :foreground "White" :weight bold))))
 '(sml/modes ((t (:inherit sml/global :background "#757B81"
			   :foreground "Black"))))
 '(sml/mule-info ((t (:inherit sml/global :background "#4E5A65"))))
 '(sml/name-filling ((t (:inherit sml/prefix :background "#757B81"
				  :weight normal))))
 '(sml/not-modified ((t (:inherit sml/global :background "#4E5A65"))))
 '(sml/numbers-separator ((t (:inherit sml/col-number :background "#4E5A65"))))
 '(sml/position-percentage ((t (:inherit sml/prefix :background "#757B81"
					 :foreground "orange" :weight normal))))
 '(sml/prefix ((t (:inherit sml/global :background "#4E5A65"
			    :foreground "orange"))))
 '(sml/process ((t (:inherit sml/prefix :background "#757B81"))))
 '(sml/remote ((t (:inherit sml/global :background "#4E5A65"))))
 '(sml/time ((t (:inherit sml/global :background "black"
			  :foreground "dark orange"))))
 '(sml/vc ((t (:inherit sml/git :background "#757B81" :foreground "#0000aa"))))
 '(sml/vc-edited ((t (:inherit sml/prefix :background "#757B81"
			       :foreground "#330000")))))

;;,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,;;
;; Smoother scroll
;;'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';;
(setq mouse-wheel-scroll-amount '(1 ((shift) . 1))) ;; one line at a time
(setq mouse-wheel-progressive-speed nil) ;; don't accelerate scrolling
(setq mouse-wheel-follow-mouse 't) ;; scroll window under mouse
(setq scroll-step 1) ;; keyboard scroll one line at a time
(setq margin 1) ;; keyboard scroll one line at a time
(require 'smooth-scroll)

;;,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,;;
;; Buffer list cleanup
;;'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';;
(add-hook 'kill-buffer-hook
	  (lambda ()
	    (setq buffer-name-history
		  (delete (buffer-name) buffer-name-history))))
(setq history-delete-duplicates t)

;;,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,;;
;; Rainbow delimiters
;;'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';;
(add-hook 'prog-mode-hook #'rainbow-delimiters-mode)

;;,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,;;
;; Powerline theme
;;'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';;
(setq powerline-default-separator 'arrow)
(setq sml/theme 'powerline)
(require 'smart-mode-line)
(sml/setup)

;;,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,;;
;; Fringe
;;'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';;
(fringe-mode '(10 . 0))

;;,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,;;
;; Line number format
;;'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';;
(setq nlinum-format "%5.5s")

;;,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,;;
;; Highlight matching bracket
;;'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';;
(show-paren-mode 1)
(setq show-paren-delay 0)
(require 'paren)
(set-face-background 'show-paren-match (face-background 'default))
(set-face-foreground 'show-paren-match "#def")
(set-face-attribute 'show-paren-match nil :weight 'extra-bold)

;;,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,;;
;; Custom bash mode, thanks gker
;;'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';;
(defun gker-setup-sh-mode ()
  "My own personal preferences for `sh-mode'.

This is a custom function that sets up the parameters I usually
prefer for `sh-mode'.  It is automatically added to
`sh-mode-hook', but is can also be called interactively."
  (interactive)
  (setq sh-basic-offset 4
	sh-indentation 4
	tab-width 4))
(add-hook 'sh-mode-hook 'gker-setup-sh-mode)

;;,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,;;
;; Git diff hl
;;'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';;
(require 'diff-hl)
(global-diff-hl-mode 1)
(diff-hl-flydiff-mode 1)
(add-hook 'dired-mode-hook 'diff-hl-dired-mode)

;;,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,;;
;; Multiple cursors
;;'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';;
(require 'multiple-cursors)
(global-set-key (kbd "C-S-c C-S-c") 'mc/edit-lines)
(global-set-key (kbd "M-<down>") 'mc/mark-next-like-this)
(global-set-key (kbd "M-<up>") 'mc/mark-previous-like-this)
(global-set-key (kbd "M-C-<down>") 'mc/mark-all-like-this)
(global-set-key (kbd "C-S-<mouse-1>") 'mc/add-cursor-on-click)

;;,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,;;
;; Disable beep
;;'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';;
(setq ring-bell-function 'ignore)

;;,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,;;
;; Stop spamming files
;;'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';;
(setq make-backup-files nil) ; stop creating backup~ files
(setq auto-save-default nil) ; stop creating #autosave# files
(setq create-lockfiles nil) ; stop hard links

;;,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,;;
;; No scroll bar in minibuffer
;;'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';;
(set-window-scroll-bars (minibuffer-window) nil nil)

;;,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,;;
;; Furter customization for linum and left margin width
;;'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';;
(eval-after-load "nlinum"
  '(set-face-attribute 'linum nil :height 80))
(set-default-font "Source Code Variable-12")

;;,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,;;
;; Emacsclient workaround for nlinum
;;'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';;
(defun initialize-nlinum (&optional frame)
  (require 'nlinum)
  (add-hook 'prog-mode-hook 'nlinum-mode))
(when (daemonp)
  (add-hook 'window-setup-hook 'initialize-nlinum)
  (defadvice make-frame (around toggle-nlinum-mode compile activate)
    (nlinum-mode -1) ad-do-it (nlinum-mode 1)))

;;,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,;;
;; Start Emacs in real fullscreen - no windowed
;;'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';;
(defun fullscreen ()
  (interactive)
  (x-send-client-message nil 0 nil "_NET_WM_STATE" 32
			 '(2 "_NET_WM_STATE_FULLSCREEN" 0)))
(fullscreen)

;;; init.el ends here
